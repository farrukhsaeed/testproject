#include <iostream>
#include <windows.h>

using namespace std;
char gameBoardArray[9][9]={' '};
int rowSize=8;
int columnSize=8;

int turn=1;

int end()
{
	return 1;
}

void moveCursor(int xpos, int ypos)//function to go to particular co-ordinates on the screen
{
    COORD scrn;
    HANDLE hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
    scrn.X = xpos;
    scrn.Y = ypos;
    SetConsoleCursorPosition(hOutput,scrn);
}

void clearUserInput(int row)

{
     HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
     COORD coord = {0, row};
     DWORD count;
     CONSOLE_SCREEN_BUFFER_INFO csbi;
     if(GetConsoleScreenBufferInfo(hStdOut, &csbi))
     {
          FillConsoleOutputCharacter(hStdOut, (TCHAR) 32, csbi.dwSize.X * (csbi.dwSize.Y - row), coord, &count);
          FillConsoleOutputAttribute(hStdOut, csbi.wAttributes, csbi.dwSize.X * (csbi.dwSize.Y - row), coord, &count );
          SetConsoleCursorPosition(hStdOut, coord);
     }
}


int options()
{
char a = 1;
 int option;
 cout<<" Lets Play gameBoardArray -- Choose from below options "<<endl<<endl;
cout<<"1: New Game"<<endl;
 cout<<"2: Quit"<<endl;
 cin>>option;
 return option;
}

void initBoardArrayWithEmptySpaces()
{
 for(int i=0;i<=rowSize;i++)
 {
  for(int j=0;j<=columnSize;j++)
   gameBoardArray[i][j]=' ';
 }
}
void initPlayerOneBoard()
{
    gameBoardArray[1][1]='R';
	gameBoardArray[1][2]='N';
	gameBoardArray[1][3]='B';
	gameBoardArray[1][4]='Q';
	gameBoardArray[1][5]='K';
	gameBoardArray[1][6]='B';
	gameBoardArray[1][7]='N';
	gameBoardArray[1][8]='R';
	for(int i=1;i<=rowSize;i++)
	{
		gameBoardArray[2][i]='P';
	}
	
}
void initPlayerTwoBoard()
{

	 gameBoardArray[rowSize][1]='r';
	 gameBoardArray[rowSize][2]='n';
	 gameBoardArray[rowSize][3]='b';
	 gameBoardArray[rowSize][4]='q';
	 gameBoardArray[rowSize][5]='k';
	 gameBoardArray[rowSize][6]='b';
	gameBoardArray[rowSize][7]='n';
	gameBoardArray[rowSize][8]='r';
	for(int i=1;i<=columnSize;i++)
	{ 
		gameBoardArray[7][i]='p';
	}
}
void initBoard()
{
	initBoardArrayWithEmptySpaces();
	initPlayerOneBoard();
	initPlayerTwoBoard();
	
}
void displayNumberAtCoordinate(int x, int y, int c)
{
	x = (x*4)-2;
    y = (y*2)+1;
    moveCursor(x,y);
    cout<<c;
	end();
}
void displayPieceAtCoordinate(int x, int y, char c)
{
	 x = (x*4)-2;
    y = (y*2)+1;
    moveCursor(x,y);
    cout<<c;
	end();
}
void displayBoard(){
	//system("color F0");
	int row=1;
    while(row<10)//this loop prints the horizontal line of board
    {
        moveCursor(4,row*2);
		int column=0;
		while(column<33)
		{
			cout<<"_";
			column++;
		}
		row++;
    }

	row=1;
    while(row<10)//this loop prints the vertical line of board
    {
     int column=3;
    while(column<19)
    {
        moveCursor(row*4,column);
        cout<<"|";
		column++;
    }
		row++;
    }
}

void displayVerticalNumbering()
{
	int number = 1;
    for(int y=1;y<=columnSize;y++)//this loop prints the co-ordinates of columns
    {
	     displayNumberAtCoordinate(1, y, number);
        number++;
     }
}

void displayHorizontalNumbring()
{
	int number = 1;
    for(int x=2;x<=rowSize+1;x++)
	{
		displayNumberAtCoordinate(x, 0, number);//this loop prints the co-ordinates of rows
        number++;
	}
}

void displayActualPieces()
{
    for(int i=1; i<=rowSize; i++)
	{
		for(int j=1; j<=columnSize; j++)
		{
			displayPieceAtCoordinate(i+1, j, gameBoardArray[j][i]);//prints the actual pieces
		}
	}
}

void displayPieces()
{

	displayVerticalNumbering();
	displayHorizontalNumbring();
	displayActualPieces();

}
bool isEmpty(int x, int y)
{
    return gameBoardArray[x][y] == ' ';
}

bool isHorizontalMove(int sourceRow, int sourceCol, int destinationRow, int destinationCol)//this function checks the horizontal move
{
	return (sourceRow == destinationRow && sourceCol != destinationCol);
}
bool isVerticalMove(int sourceRow, int sourceCol, int destinationRow, int destinationCol)//this function checks the vertical move
{
	return (sourceCol == destinationCol && sourceRow != destinationRow);
}
bool isDaigonalMove(int sourceRow, int sourceCol, int destinationRow, int destinationCol)//checks the diagonal move
{ 
	return ( abs(sourceRow-destinationRow) == abs(sourceCol-destinationCol) );
}

bool isHorizontalPathClear(int sourceRow, int sourceColumn, int destinationColumn)//whether the way of piece moving horizontally is clear or not
{
	int i,j;
	if(sourceColumn<destinationColumn)
	{
		i = sourceColumn+1;
		j = destinationColumn-1;
	}
	else
	{
		i = destinationColumn+1;
		j = sourceColumn-1;
	}

	for( i ;  i<=j;  i++)
	{
		if(	gameBoardArray[sourceRow][i] != ' ' )
			return false;
	}
	return true;
}
bool isVerticalPathClear(int sourceRow, int sourceColumn, int desitinationRow)//whether the way of piece moving vertically is clear or not
{
	int i,j;
	if(sourceRow<desitinationRow)
	{
		i = sourceRow+1;
		j = desitinationRow-1;
	}
	else
	{
		i = desitinationRow+1;
		j = sourceRow-1;
	}

	for(i; i<=j; i++)
	{
		if( gameBoardArray[i][sourceColumn] != ' ')
			return false;
	}
	return true;
	
}
bool upwardDialogCheck(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	int i, j, k, l;
	if(sourceRow<desitinationRow)
	{
		i = sourceRow+1;
		j = sourceColumn+1;
		k = desitinationRow-1;
		l = destinationColumn-1;
	}
	else
	{
		i = desitinationRow+1;
		j = destinationColumn+1;
		k = sourceRow-1;
		l = sourceColumn-1;
	}
	if(abs(desitinationRow-sourceRow)==1 && abs(sourceColumn-destinationColumn) == 1)
		return true;
	else
	{
		for(i,j ; i<=k, j<=l ; i++, j++)
		{
			if( gameBoardArray[i][j]!= ' ')
				return false;
		}
		return true;
	}
}
bool downwardDiagonal(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	int i, j, k , l;
	if(sourceRow<desitinationRow)
	{
		i = sourceRow+1;
		j = sourceColumn-1;
		k = desitinationRow-1;
		l = destinationColumn+1;
	}
	else
	{
		i = desitinationRow+1;
		j = destinationColumn-1;
		k = sourceRow-1;
		l = sourceColumn+1;
	}

	if(abs(desitinationRow-sourceRow)==1 && abs(sourceColumn-destinationColumn) == 1)
		return true;
	else
	{
		for(i,j ; i<=k, j>=l ; i++, j--)//
		{
			if( gameBoardArray[i][j]!= ' ')
				return false;
		}
		return true;
	}
	
}



bool isDiagonalPathClear(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)//checks the diagonal way is clear or not
{
	if( (sourceRow<desitinationRow && sourceColumn<destinationColumn) || (desitinationRow<sourceRow && destinationColumn<sourceColumn) ) 
		return upwardDialogCheck(sourceRow, sourceColumn, desitinationRow, destinationColumn);
	else if( ( sourceRow<desitinationRow && sourceColumn>destinationColumn ) || (sourceRow>desitinationRow && sourceColumn<destinationColumn) )
		return downwardDiagonal(sourceRow, sourceColumn, desitinationRow, destinationColumn);
}

//legality functions of all pieces
bool validateRookMove(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	if( isHorizontalPathClear(sourceRow, sourceColumn, destinationColumn) && isVerticalPathClear(sourceRow, sourceColumn, desitinationRow) )
		return ( isHorizontalMove(sourceRow, sourceColumn, desitinationRow, destinationColumn) ||  isVerticalMove( sourceRow, sourceColumn, desitinationRow, destinationColumn) );
	else
		return false;
}
bool validateKnightMove(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	if( abs(sourceRow-desitinationRow )==2 && abs(sourceColumn-destinationColumn)==1	)
		return true;
	else if( abs(sourceRow-desitinationRow )==1 && abs(sourceColumn-destinationColumn)==2)
		return true;
	else
		return false;
}
bool validateBishopMove(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	if(isDiagonalPathClear(sourceRow, sourceColumn, desitinationRow, destinationColumn) )
			return isDaigonalMove(sourceRow, sourceColumn, desitinationRow, destinationColumn);
	else
		return false;	
}
bool validateQueenMove(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	if(isHorizontalMove(sourceRow, sourceColumn, desitinationRow, destinationColumn))
		return isHorizontalPathClear(sourceRow, sourceColumn, destinationColumn);
	else if ( isVerticalMove(sourceRow, sourceColumn, desitinationRow, destinationColumn) )
		return isVerticalPathClear(sourceRow, sourceColumn, desitinationRow);
	else if ( isDaigonalMove(sourceRow, sourceColumn, desitinationRow, destinationColumn) )
		return isDiagonalPathClear(sourceRow, sourceColumn, desitinationRow, destinationColumn);
}
bool validateKingMove(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	if(sourceRow == desitinationRow && abs(sourceColumn-destinationColumn) == 1)
		return true;
	else if(sourceColumn == destinationColumn && abs(sourceRow-desitinationRow) == 1)
		return true;
	else if(abs(sourceRow-desitinationRow) == 1 && abs(sourceColumn-destinationColumn) ==1 )
		return true;
	else
		return false;
}
bool isValidMoveOfBlackPawn(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{	
	if(isVerticalPathClear(sourceRow,sourceColumn,desitinationRow) == true )
		return(desitinationRow>sourceRow && sourceColumn==destinationColumn);
}
bool isValidMoveOfWhitePawn(int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)
{
	if(isVerticalPathClear(sourceRow,sourceColumn,desitinationRow) == true)
		return(sourceRow>desitinationRow && sourceColumn==destinationColumn);
}

bool isSourcePositionValid(int player, char piece)//this function checks if the selected piece is of the player or not
{
	if(player == 1)
		return (piece>= 'A' && piece <= 'Z');
	else
		return (piece>= 'a' && piece <= 'z');
}
bool isChoosenPawn(int player, int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)//checks whether the destination is empty and whether the player selected his own piece or not
{
	if( abs(sourceRow-desitinationRow)==1 && abs(sourceColumn-destinationColumn)==1 )
		return ( !isSourcePositionValid(player, gameBoardArray[desitinationRow][destinationColumn] ) && gameBoardArray[desitinationRow][destinationColumn]!= ' ');
	else
		return false;
}

bool moveOfPawn(int sourceRow, int desitinationRow)//whether the pawn is moving two or one steps on first turn and moving only one step in next turns
{
	if( sourceRow == 2 || sourceRow == 7)
	{		if ( abs(sourceRow-desitinationRow)<=2 )
		{
			return true;
		}
		else
			return false;}

	else

	{
		if ( abs(sourceRow-desitinationRow) == 1 )
			return true;
		else
			return false;
	}
}
void movePiece(char piece, int sourceRow, int sourceColumn, int desitinationRow, int destinationColumn)//moves the piece from current position to the required position
{
		gameBoardArray[sourceRow][sourceColumn] = ' ';
		gameBoardArray[desitinationRow][destinationColumn] = piece;
}



bool isValidMove(char piece, int sourceRow, int sourceColumn, int destinationRow, int destinationColumn, bool val)
{
	int tempPlayer;
	switch(piece)
	{
		case 'R':
		case 'r':
			return validateRookMove(sourceRow, sourceColumn, destinationRow,  destinationColumn);
			break;

		case 'N':
		case 'n':
			return validateKnightMove(sourceRow, sourceColumn, destinationRow,  destinationColumn);
			break;

		case 'B':
		case 'b':
			return validateBishopMove(sourceRow, sourceColumn, destinationRow,  destinationColumn);
			break;

		case 'Q':
		case 'q':
			return validateQueenMove(sourceRow, sourceColumn, destinationRow,  destinationColumn);
			break;

		case 'K':
		case 'k':
			return validateKingMove(sourceRow, sourceColumn, destinationRow,  destinationColumn);
			break;

		case 'P':
			tempPlayer = 1;
			if(val == true)
				return ((isValidMoveOfBlackPawn(sourceRow, sourceColumn, destinationRow, destinationColumn) && moveOfPawn(sourceRow, destinationRow) && isEmpty(destinationRow, destinationColumn)) || isChoosenPawn(tempPlayer, sourceRow, sourceColumn, destinationRow, destinationColumn));
			else
				return (isValidMoveOfBlackPawn(sourceRow, sourceColumn, destinationRow,  destinationColumn) && moveOfPawn(sourceRow,destinationRow) && isEmpty(destinationRow, destinationColumn));			
			break;

		case 'p':
			tempPlayer = 2;
			if(val==true)
				return ((isValidMoveOfWhitePawn(sourceRow, sourceColumn, destinationRow, destinationColumn) && moveOfPawn(sourceRow, destinationRow) && isEmpty(destinationRow, destinationColumn)) || isChoosenPawn(tempPlayer, sourceRow, sourceColumn, destinationRow, destinationColumn));
			else
				return (isValidMoveOfWhitePawn(sourceRow, sourceColumn, destinationRow,  destinationColumn) && moveOfPawn(sourceRow, destinationRow) && isEmpty(destinationRow, destinationColumn));
			break;
	}
}
bool isSourcePositionValid(int player, char piece, int sourceRow, int sourceColumn)//checks whether the piece at source position is of the player whose turn it is or not
{
	if(player == 1)
	{
		if(gameBoardArray[sourceRow][sourceColumn] == ' ')
			return false;
		else 
			return (piece>= 'A' && piece <= 'Z');
	}
	else
	{
		if(gameBoardArray[sourceRow][sourceColumn] == ' ')
			return false;
		else 
			return (piece>= 'a' && piece <= 'z');
	}
}
bool isDestinationPlaceEmpty(int player, int destinationRow, int destinationColumn)//whether the piece at destination position is of the opposite player or the destination position is empty 
{
	if(player == 1)
		return (  (gameBoardArray[destinationRow][destinationColumn]>='a' && gameBoardArray[destinationRow][destinationColumn]<='z') || gameBoardArray[destinationRow][destinationColumn] == ' '  );
	else
		return (  (gameBoardArray[destinationRow][destinationColumn]>='A' && gameBoardArray[destinationRow][destinationColumn]<='Z') || gameBoardArray[destinationRow][destinationColumn] == ' '  );
}
void switchTurn(int & player)
{
	if (player==1)
	{   player =2 ;
	}
	else  if ( player ==2)
	{ player= 1;}
	
}

bool isMoveInBoundary(int sourceRow, char sourceColumn, int destinationRow, char destinationColumn)
{
	return( !(sourceRow<1 || sourceRow>8 || destinationRow<1 || destinationRow>8  ));
}

void pieceBackToInitPos(char piece, int sourceRow, int sourceColumn, int destinationRow, int destinationColumn)//this is to move the piece back to the initial position
{
	gameBoardArray[sourceRow][sourceColumn] = gameBoardArray[destinationRow][destinationColumn];
	gameBoardArray[destinationRow][destinationColumn] = piece;
}

bool canPawnKill(int player, int x, int y)
{
	int playeropp;
	if ( player ==1 )
	{ playeropp=2; }
	else { playeropp=1;}
	if(playeropp == 1)
		return ( gameBoardArray[x-1][y-1] == 'P' ||  gameBoardArray[x-1][y+1] == 'P'  );
	else
		return ( gameBoardArray[x+1][y-1] == 'p' ||  gameBoardArray[x+1][y+1] == 'p'  );
}


void takingInputForMove(int & sourceRow, int & sourceColumn, int & destinationRow, int & destinationColumn, int  player, char & piece)
{
	moveCursor(0,20);
	cout<<"TURN: "<<turn<<" .Player "<<player<<"'s turn."<<endl;
	cout<<"Enter piece to move:";
	
	cin>>piece;
	end();
	if(piece=='R'||piece=='N'||piece=='B'||piece=='Q'||piece=='K'||piece=='P'||piece=='r'||piece=='n'||piece=='b'||piece=='q'||piece=='k'||piece=='p')
	{
		cout << " give input (sourceRow,sourceColumn,destinationRow,destinationColumn)"<<endl;
		cin>> sourceRow>>sourceColumn>>destinationRow>>destinationColumn;
		
		end();
	}
	else
	{
		sourceRow=0, sourceColumn=0, destinationRow=0, destinationColumn=0,player=1;
	}
}
void InitializeAllVariables(int&player,int&sourceRow,int&sourceColumn,int&destinationRow,int&destinationColumn,char&sourceEmptyColumn,char&destinationEmptyColumn,char&piece,bool &warn,bool&threat,int&playeropp,bool&checkmate,bool&stalemate,bool&onlyLegalCheck,bool load)
{
	if(load==true)
		player=player;
	else
		player =1;
	sourceRow=0, sourceColumn=0, destinationRow=0, destinationColumn=0;
	 sourceEmptyColumn=' ', destinationEmptyColumn=' ';
	piece=' ';
	
	playeropp;
	checkmate = false;
	stalemate = false;
	onlyLegalCheck = false;
}



void mainCode(int&player,int&sourceRow,int&sourceColumn,int&destinationRow,int&destinationColumn,char&sourceEmptyColumn,char&destinationEmptyColumn,char&piece,bool &warn,bool&threat,int&playeropp,bool&checkmate,bool&stalemate,bool&onlyLegalCheck, bool load)
{
	string che;
	displayBoard();
	end();
	if(load == false)
	{
		initBoard();
	}
	
	
	displayPieces();
	bool val= false;
	playeropp=player;
	

	beg:while(true)
	{	
		if(load==false)
		{		
			if  (    player ==1     ) 
		{   playeropp=2;}
		else {playeropp=1;}}
		else
			load=false;

		//take input from the user if the piece is under check after any move 
		takingInputForMove(sourceRow,sourceColumn,destinationRow,destinationColumn,player,piece);
		end();
			
		
		if( isMoveInBoundary(sourceRow,sourceEmptyColumn,destinationRow,destinationEmptyColumn) )
		{
			if( isSourcePositionValid(player, piece, sourceRow, sourceColumn))//Enter this loop if the player has selected his own piece
			{
				onlyLegalCheck = true;
				if(	isValidMove(piece, sourceRow, sourceColumn, destinationRow, destinationColumn, onlyLegalCheck) )//Enter this loop after assuring that the move user wants to make is legal
				{
					onlyLegalCheck = false;
					if( isDestinationPlaceEmpty(player, destinationRow, destinationColumn) )//Enter this loop if the destination place is empty or has the opponent's piece
					{	
							
							
							movePiece(piece, sourceRow, sourceColumn, destinationRow, destinationColumn);					
							
							//transformationOfPawn(gameBoardArray[destinationRow][destinationColumn], destinationRow);
							displayPieces();
							switchTurn(player);
							turn++;
							clearUserInput(20);
													
						if ( player==1 )
						{ playeropp =2;}
						else 
						{ playeropp =1 ;}
						
					}
					else // Enter this else if the user tries to place his own piece at his any other piece
					{
						cout<<"You cannot place a piece at the position of your another piece.";
						Sleep(1000);
						clearUserInput(20);
					}
				}
				else // Enter this else if the move of player is illegal
				{
					cout<<"Illegal Move";
					Sleep(1000);
					clearUserInput(20);

				}
			}
			else // Enter this else if player tries to move the piece of other player or moves an empty place
			{
				cout<<"This is not your piece.Plz select your own piece";
				Sleep(1000);
				clearUserInput(20);
			}		
		}
		else
		{
			cout<<"Invalid Coordinates!!";
			Sleep(1000);
			clearUserInput(20);
		}	
		end();
		//SaveGame(player,turn);
		//finalSave(player,turn);
		
	}
	
	
}

int main()
{
	//welcome();
    int option = options();
	system("cls");
    int player,sourceRow, sourceColumn, destinationRow, destinationColumn, playeropp;	//player 1 has capital letters
	char sourceEmptyColumn, destinationEmptyColumn, piece;
	bool  warn, threat, checkmate, stalemate, onlyLegalCheck ;
	bool load = false;
	
switch(option)
{ 
 case 1://to start a new game
 { 
  InitializeAllVariables(player, sourceRow, sourceColumn, destinationRow, destinationColumn, sourceEmptyColumn, destinationEmptyColumn, piece, warn, threat, playeropp, checkmate, stalemate, onlyLegalCheck,load);
  mainCode(player, sourceRow, sourceColumn, destinationRow, destinationColumn, sourceEmptyColumn, destinationEmptyColumn, piece, warn, threat, playeropp, checkmate, stalemate, onlyLegalCheck,load);
  //SaveGame(player,turn);

  Sleep(2000);

  break;}

 case 2:

	 { return 0;}

}
	system("pause");
    return 0;
}